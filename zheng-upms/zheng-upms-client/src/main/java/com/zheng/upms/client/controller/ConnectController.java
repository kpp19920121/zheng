package com.zheng.upms.client.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 判断系统的联通性 controller
 * 
 * @author kefan
 *
 */
@Controller
@RequestMapping("/sso")
public class ConnectController {

	
	/**
	 * ajax跨域请求，需要使用jquery的jsonp方法
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping("/connect")
	public void isConnection(HttpServletRequest request,HttpServletResponse response) throws IOException {
		
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=UTF-8");
		
		
		String callback=request.getParameter("callback");

		Map<String, Object> resultMap = new HashMap<String, Object>();

		resultMap.put("result", "OK");
		
		
		String result=callback+"("+JSONObject.fromObject(resultMap).toString()+")";
		
		response.getWriter().print(result);

	}

}
