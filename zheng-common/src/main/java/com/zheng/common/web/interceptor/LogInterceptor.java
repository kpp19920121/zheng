package com.zheng.common.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

public class LogInterceptor implements HandlerInterceptor {
	
	private static final Logger logger=LoggerFactory.getLogger(LogInterceptor.class);
	@Override
	public void afterCompletion(HttpServletRequest arg0,
			HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response,
			Object hanlder, ModelAndView modelAndView) throws Exception {
		if(modelAndView!=null){
			logger.debug("redirect view:"+modelAndView.getViewName());
		}
		
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
			Object handler) throws Exception {
		
		if(handler instanceof HandlerMethod){
			HandlerMethod handlerMethod=(HandlerMethod)handler;
			logger.debug("invoke class:"+handlerMethod.getBeanType());
			logger.debug("invoke method:"+handlerMethod.getMethod());
		}else{
			logger.debug(handler.getClass().getName());
		}
		
		return true;
	}

}
